       IDENTIFICATION DIVISION.
       PROGRAM-ID. MY-GRADE.
       AUTHOR. THUN.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT MY-GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUeNTIAL.
           SELECT AVG-FILE ASSIGN TO "AVG.txt"
              ORGANIZATION IS LINE SEQUeNTIAL.
       DATA DIVISION.
       FILE SECTION.
       FD  MY-GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-SCORE-FILE VALUE  HIGH-VALUE .
           05 COURSE-CODE PIC X(6).
           05 COURSE-NAME PIC X(50).
           05 CREDIT      PIC 9.
           05 GRADE       PIC X(2).

       FD  AVG-FILE.
       01  ALL-AVG-GRADE.
           05 AVG-GRADE            PIC 9.999.
           05 SCI-AVG-GRADE        PIC 9.999.
           05 COM-SCI-AVG-GRADE    PIC 9.999.

       WORKING-STORAGE SECTION. 
       01  ALL-GRADE            PIC 9(3)V9.
       01  AVG-ALL-GRADE        PIC 9v999.
       01  ALL-CREDIT           PIC 9(3).
       01  SCI-GRADE            PIC 9(3)V9.
       01  AVG-SCI-GRADE        PIC 9v999.
       01  SCI-CREDIT       PIC 9(3).
       01  COM-SCI-GRADE        PIC 9(3)V9.
       01  AVG-COM-SCI-GRADE    PIC 9v999.
       01  COM-SCI-CREDIT   PIC 9(3).

       
       PROCEDURE DIVISION .
       000-BEGIN.
           OPEN INPUT MY-GRADE-FILE
           OPEN OUTPUT AVG-FILE
           PERFORM  UNTIL END-OF-SCORE-FILE
              DISPLAY COURSE-CODE SPACE COURSE-NAME SPACE CREDIT
                 SPACE GRADE
              READ MY-GRADE-FILE 
                 AT END SET END-OF-SCORE-FILE TO TRUE 
              END-READ
              IF NOT END-OF-SCORE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF
              
           END-PERFORM
           COMPUTE AVG-ALL-GRADE = ALL-GRADE / ALL-CREDIT 
           COMPUTE AVG-SCI-GRADE = (SCI-GRADE+COM-SCI-GRADE)/ SCI-CREDIT  
           COMPUTE AVG-COM-SCI-GRADE = COM-SCI-GRADE / COM-SCI-CREDIT
           MOVE AVG-ALL-GRADE TO AVG-GRADE 
           MOVE AVG-SCI-GRADE TO SCI-AVG-GRADE
           MOVE AVG-COM-SCI-GRADE TO COM-SCI-AVG-GRADE
           WRITE ALL-AVG-GRADE
           CLOSE  MY-GRADE-FILE
           CLOSE  AVG-FILE
           GOBACK
           .
       001-PROCESS.
           EVALUATE TRUE
              WHEN GRADE = "A" COMPUTE ALL-GRADE = ALL-GRADE+(4*CREDIT)
              WHEN GRADE = "B+" COMPUTE ALL-GRADE=ALL-GRADE+(3.5*CREDIT)
              WHEN GRADE = "B" COMPUTE ALL-GRADE = ALL-GRADE +(3*CREDIT)
              WHEN GRADE = "C+" COMPUTE ALL-GRADE=ALL-GRADE+(2.5*CREDIT)
              WHEN GRADE = "C" COMPUTE ALL-GRADE = ALL-GRADE+(2*CREDIT)
              WHEN GRADE = "D+" COMPUTE ALL-GRADE=ALL-GRADE+(1.5*CREDIT)
              WHEN OTHER COMPUTE ALL-GRADE = ALL-GRADE + CREDIT
           END-EVALUATE
           IF COURSE-CODE (1:1) IS EQUAL TO "3" THEN
              IF COURSE-CODE (1:2) IS EQUAL TO "31" THEN 
                 COMPUTE COM-SCI-CREDIT=COM-SCI-CREDIT+CREDIT
                 COMPUTE SCI-CREDIT = SCI-CREDIT + CREDIT
                 EVALUATE TRUE
                    WHEN GRADE = 
                    "A" COMPUTE COM-SCI-GRADE = COM-SCI-GRADE+(4*CREDIT)

                    WHEN GRADE = 
                    "B+"COMPUTE COM-SCI-GRADE=COM-SCI-GRADE+(3.5*CREDIT)
                    WHEN GRADE = 
                    "B" COMPUTE COM-SCI-GRADE = COM-SCI-GRADE+(3*CREDIT)
                    WHEN GRADE = 
                    "C+"COMPUTE COM-SCI-GRADE=COM-SCI-GRADE+(2.5*CREDIT)
                    WHEN GRADE = 
                    "C" COMPUTE COM-SCI-GRADE = COM-SCI-GRADE+(2*CREDIT)
                    WHEN GRADE = 
                    "D+"COMPUTE COM-SCI-GRADE=COM-SCI-GRADE+(1.5*CREDIT)
                    WHEN OTHER COMPUTE 
                    COM-SCI-GRADE=COM-SCI-GRADE+CREDIT
                 END-EVALUATE 
              ELSE  
                 COMPUTE SCI-CREDIT = SCI-CREDIT + CREDIT
                 EVALUATE TRUE
                    WHEN GRADE = 
                    "A" COMPUTE SCI-GRADE = SCI-GRADE+(4*CREDIT)
                    WHEN GRADE = 
                    "B+"COMPUTE SCI-GRADE=SCI-GRADE+(3.5*CREDIT)
                    WHEN GRADE = 
                    "B" COMPUTE SCI-GRADE = SCI-GRADE+(3*CREDIT)
                    WHEN GRADE = 
                    "C+"COMPUTE SCI-GRADE=SCI-GRADE+(2.5*CREDIT)
                    WHEN GRADE = 
                    "C" COMPUTE SCI-GRADE = SCI-GRADE+(2*CREDIT)
                    WHEN GRADE = 
                    "D+"COMPUTE SCI-GRADE=SCI-GRADE+(1.5*CREDIT)
                    WHEN OTHER COMPUTE 
                    SCI-GRADE=SCI-GRADE+CREDIT
                 END-EVALUATE 
              END-IF 
           END-IF 
           COMPUTE ALL-CREDIT = ALL-CREDIT + CREDIT 
           .
       
       001-EXIT.
           EXIT
           .
